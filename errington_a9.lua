------------------------------------------------------
----------------------Shape---------------------------
Shape = {}
Shape.__index = Shape

--Constructor
function Shape.new(filled,color)
	local instance = {
		_filled = filled,
		_color = color
	}

	setmetatable(instance, Shape)
	return instance
end

-- Get color
function Shape:getColor()
	return self._color;
end

-- Get filled
function Shape:getFilled()
	return self._filled;
end

--Set color
function Shape:setColor(newColor)
	self._color = newColor;
end

--Set filled
function Shape.setFilled(newFill)
	self._filled = newFill;
end

--to string
function Shape:toString()
	print("Filled:", self:getFilled(), "Color:", self:getColor())
end

------------------------------------------------------
---------------------Circle---------------------------

Circle = {}
Circle.__index = Circle

--Constructor
function Circle.new(filled, color,radius)
	local instance = {
		_radius = radius,
		_filled = filled,
		_color = color,
	}

	setmetatable(instance, Circle)
	return instance
end

setmetatable(Circle,{__index = Shape})

--Get radius
function Circle:getRadius()
	return self._radius;
end

--Get area
function Circle:getArea()
	return (self._radius * self._radius)*(3.14)
end

-- Get perimiter
function Circle:getPerimeter()
	return (self._radius * 6.28)
end

-- Set Radius
function Circle:setRadius(newRadius)
	self._radius = newRadius
end

-- To string
function Circle:toString()
	print("Filled:", self:getFilled(), "Color:", self:getColor(),
		  "Radius:", self:getRadius(), "Area:", self:getArea(),
		  "Perimiter:", self:getPerimeter())
end

------------------------------------------------------
-------------------Rectangle--------------------------
Rectangle = {};
Rectangle.__index = Rectangle
--constructor
function Rectangle.new(filled, color, width, length)
	local instance = {
		_filled = filled,
		_color = color,
		_width = width,
		_length = length,
	}

	setmetatable(instance, Rectangle)
	return instance
end

setmetatable(Rectangle, {__index, Shape})

-- Get Width
function Rectangle:getWidth()
	return self._width
end

-- Get lenght
function Rectangle:getLength()
	return self._length
end

--get area
function Rectangle:getArea()
	return (self._length * self._width)
end

--get perimeter
function Rectangle:getPerimeter()
	return ((self._length * 2) + (self.width * 2))
end

-- set width
function Rectangle:setWidth(newWidth)
	self._width = newWidth
end

-- set length
function Rectangle:setLength(newLength)
	self._length = newLength
end

-- to string
function Rectangle:toString()
	print("Filled:", self:getFilled(), "Color:", self:getColor(),
		  "Width:", self:getWidth(), "Length:", self:getLength(),
		  "Area:", self:getArea(), "Perimiter:", self:getPerimeter())
end


------------------------------------------------------
---------------------Square---------------------------
Square = {}
Square.__index = Square
--constructor
function Square(filled, color, side)
	local instance = {
		_filled = filled,
		_color = color,
		_side = side,
	}

	setmetatable(instance, Square)
	return instance
end

setmetatable(Square, {__index = Rectangle})

-- Get Side
function Square:getSide()
	return self._side;
end

-- Set side
function Square:setSide(newSide)
	self._side = newSide
end

------------------------------------------------------
---------------------Main-----------------------------

-- Testing the shape base class
print("Shape tests----------------")
local shape = Shape.new(true, "red")
print(shape:getFilled())
print(shape:getColor())
shape:toString();
--[[shape.setColor("blue")
shape.setFilled(false)
shape:toString();--]]

-- Testing the circle class
print("Circle tests---------------")
local circle = Circle.new(true, "red", 3)
print(circle:getFilled())
print(circle:getColor())
print(circle:getRadius())
print(circle:getArea())
print(circle:getPerimeter())
circle:toString();
circle:setRadius(15)
circle:toString();
--circle.Shape:setFilled(false)
--circle:setColor("brown")
--circle:toString();

-- Testing the Rectangle class
print("Rectangle tests-------------")